#!/usr/bin/python

'''
programmer  : Abolfazl Ziaeemehr
Date        : 11/2018
Department of Physics,
PhD student in Computational Physics
Institute for Advanced Studies in Basic Sciences (IASBS)
Tel: a.ziaeemehr@gmail.com [@iasbs.ac.ir]
Page: iasbs.ac.ir/~a.ziaeemehr
'''

import numpy as np
from scipy.integrate import odeint
from scipy.optimize import fsolve
import pylab as pl
from sys import exit
from copy import copy
from numpy import pi, exp


# -------------------------------------------------------- #
def find_peaks(x):
    '''
    return indices of peaks on array x
    '''
    p = []
    for i in range(1, len(x) - 1):
        if ((x[i] > x[i - 1]) & (x[i] > x[i + 1])):
            p.append(i)
    return p
# -------------------------------------------------------- #


class wang_buszaki:

    Cm = 1.0

    E_L = -65.0
    E_Na = 55.0
    E_K = -90.0

    g_L = 0.1
    g_Na = 35.0
    g_K = 9.0

    I_app = 1.0
    phi = 5.0
    dt = 0.01

    limit_cycle_found = False
    inital_conditions = [np.random.uniform(-60.0, -55.0)] +\
        np.random.rand(2).tolist()

    def __init__(self):
        pass

    # -------------------------------------------------------- #
    def set_params(self, **par):

        self.k = par["k"]
        self.phi = par['phi']
        self.I_app = par['I_app']
        self.perturb_amp = par['amplitude']

    # ----- WB Model ------------------------------- #
    def ode_sys(self, x0, t, Iext):
        '''
        define Wang Buszaki Model
        '''
        v, h, n = x0

        alpha_m = -0.1*(v+35.0)/(exp(-0.1*(v+35.0))-1.0)
        alpha_h = 0.07*exp(-(v+58.0)/20.0)
        alpha_n = -0.01*(v+34.0)/(exp(-0.1*(v+34.0))-1.0)

        beta_m = 4.0*exp(-(v+60.0)/18.0)
        beta_h = 1.0/(exp(-0.1*(v+28.0))+1.0)
        beta_n = 0.125*exp(-(v+44.0)/80.0)

        m = alpha_m/(alpha_m+beta_m)
        I_Na = self.g_Na * m*m*m * h * (v-self.E_Na)
        I_L = self.g_L*(v-self.E_L)
        I_K = self.g_K * n*n*n*n * (v-self.E_K)

        vp = -I_Na - I_K - I_L + self.I_app
        hp = self.phi * (alpha_h * (1-h) - beta_h * h)
        np = self.phi * (alpha_n * (1-n) - beta_n * n)

        return [vp, hp, np]

    # -------------------------------------------------------- #

    def find_frequencies(self, x, y):
        """
        find frequency from inter spike interval
        """
        peak = find_peaks(y)  # index of peaks
        try:
            period = x[peak[-1]] - x[peak[len(peak) - 2]]
            freq = 1.0 / period
        except:
            freq = 0.0
            print 'non oscillatory waveform or contains'
            print 'less that leat 3 periods of oscillation'
        return freq

    # -------------------------------------------------------- #
    def find_limit_cycle(self, t, x0=None, Iext=None, **keywords):
        """
        find a point on limit cycle
        return : x on limit cycle and the period of oscillation
        """
        if x0 is None:
            x0 = self.inital_conditions
        if Iext is None:
            Iext = self.I_app

        lc = odeint(self.ode_sys, x0, t, args=(Iext,))

        # pl.plot(t,lc[:,0])
        # pl.show()
        # exit(0)

        self.frequency = self.find_frequencies(t, lc[:, 0])

        indx_peak = find_peaks(lc[:, 0])  # index of peaks

        # choose a point o limit cycle (on the peak of k'th spikes befor the last one).
        self.x0_on_lc = lc[indx_peak[len(indx_peak) - int(self.k)]]
        self.period = 1.0 / self.frequency

        self.limit_cycle_found = True

    # -------------------------------------------------------- #
    def run(self, num_phase_point=200):

        if not self.limit_cycle_found:
            self.find_limit_cycle(np.arange(0, 1000, 0.01),
                                  self.inital_conditions,
                                  self.I_app)
        dt = self.dt
        t = np.arange(0, self.k * self.period + dt, dt)
        tc = np.arange(0, self.k * self.period + dt, dt)

        lc = odeint(self.ode_sys, self.x0_on_lc, t, args=(
            self.I_app,))    # find limit cycle lc[nstep x 2]

        peak = find_peaks(lc[:, 0])
        t_ref = tc[peak[-1]]      # the last peak is used for reference

        phases = np.linspace(0, self.period, num_phase_point)
        prc = np.zeros(len(phases))

        for i in range(len(phases)):
            tmp_phase = abs(phases[i] - tc)
            m, j = min(tmp_phase), np.argmin(tmp_phase)

            t = np.arange(phases[i], self.k * self.period, dt)
            x = odeint(self.ode_sys, lc[j, :] +
                       self.perturb_amp, t, args=(self.I_app,))
            peaks = find_peaks(x[:, 0])
            prc[i] = ((self.period / 2.0 + t_ref - t[peaks[-1]]) %
                      self.period) - self.period / 2.0

        # normalizing the prc to [0, 1] and rescaling the phases to [0, 2pi]
        self.prc = prc / 100.0  # / np.max(np.abs(prc))
        self.phases = phases / (np.max(np.abs(phases))) * 2.0 * pi

        # save the results
        # np.savez("prc-"+str('%.6f' % self.I_app),
        #          phase=self.phases, prc=self.prc,
        #          I=self.I_app, period=self.period)

        return {"phase": self.phases,
                "prc": self.prc,
                "frequency": (self.frequency*1000.0)}

    # --------------------------------------------------------- #
    def visualize(self):

        fig, ax = pl.subplots(1, figsize=(7, 6))
        ax.plot(self.phases, self.prc, lw=3, c='k')
        ax.set_xlabel('phase angle(rad)', fontsize=16)
        ax.set_ylabel('PRC', fontsize=16)

        try:
            ax.set_facecolor(pl.cm.gray(.95))
        except:
            ax.set_axis_bgcolor(pl.cm.gray(.95))

        # pl.rc('text', usetex = True)
        ax.set_xticks([0, pi / 2, pi, 3 * pi / 2, 2 * pi])
        ax.set_xticklabels(['$0$', r'$\frac{\pi}{2}$', r'$\pi$',
                            r'$\frac{3\pi}{2}$', r'$2\pi$'])
        ax.grid(linestyle="--")
        ax.set_title(" Wang-Buszaki Model ", size=12)
        pl.savefig('prc.png')

    # --------------------------------------------------------- #
