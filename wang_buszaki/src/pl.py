import numpy as np
import pylab as pl
from sys import exit
from copy import copy
from numpy import pi
from cycler import cycler




fig, ax = pl.subplots(1, figsize=(9, 6))
data  = np.load("data/data.npz")
phase = data['phase']
prc   = data['prc']
I_app = data['I']
phi   = data['phi']
frequency=data['frequency']

NUM_COLORS = len(I_app)
cm = pl.get_cmap('gist_rainbow')  # , gist_yarg
ax.set_prop_cycle(cycler('color',
        [cm(1.*ii/NUM_COLORS) for ii in range(NUM_COLORS)]))


for i in range(len(I_app)):
    label = str('I=%5.2f, f=%.1f[Hz]' % (I_app[i], frequency[i]))
    ax.plot(phase[i], prc[i], lw=3, label=label)

ax.set_xlabel('phase angle(rad)', fontsize=16)
ax.set_ylabel('PRC', fontsize=16)
try:
    ax.set_facecolor(pl.cm.gray(.95))
except:
    ax.set_axis_bgcolor(pl.cm.gray(.95))
ax.set_xticks([0, pi / 2, pi, 3 * pi / 2, 2 * pi])
ax.set_xticklabels(['$0$', r'$\frac{\pi}{2}$', r'$\pi$',
                    r'$\frac{3\pi}{2}$', r'$2\pi$'])
ax.grid(linestyle="--")
ax.set_title(" Wang-Buszaki Model ", size=12)
ax.legend(loc='best', frameon=False, fontsize=12)
pl.savefig('prc.png')
pl.show()
