#!/usr/bin/python

"""
Phase response curve of Wang-Buszaki Model
"""
import sys
import lib
import numpy as np
from scipy.integrate import odeint
import pylab as pl
from math import pi
from sys import exit
from time import time
from cycler import cycler


# --------------------------------------------------------------#
params = {
    "k": 4,
    "I_app": 1.0,
    "phi": 1.0,
    "amplitude": [0.8, 0.0, 0.0],
}

I_app = [1.0]  # [1.0, 0.7, 0.3]
phi = [0.5]  # , 0.7, 1.0]
variables = zip(I_app, phi)

if __name__ == '__main__':

    start = time()

    fig, ax = pl.subplots(1, figsize=(9, 6))
    colors = pl.cm.brg(np.linspace(0, 1, len(variables)))

    DATA = {}

    for i, j in variables:
        print i, j
        params["I_app"] = i
        params["phi"] = j
        sol = lib.wang_buszaki()
        sol.set_params(**params)
        S = sol.run()

        label = str(r'$I$=%5.2f, $\phi$=%.2f, f=%.1f[Hz]' % (
            i, j, S['frequency']))
        ax.plot(S['phase'], S['prc'], lw=3, label=label)

        phases = S['phase']
        prcs = S['prc']
        frequency = S['frequency']
        DATA[(i, j)] = [S['prc'], S['phase'], S['frequency']]

    np.savez('../data/data', DATA=DATA)
    print "Time = %g seconds" % (time()-start)

    ax.set_xlabel('phase angle(rad)', fontsize=16)
    ax.set_ylabel('PRC', fontsize=16)

    try:
        ax.set_facecolor(pl.cm.gray(0.95))
    except:
        ax.set_axis_bgcolor(pl.cm.gray0(.95))

    ax.set_xticks([0, pi / 2, pi, 3 * pi / 2, 2 * pi])
    ax.set_xticklabels(['$0$', r'$\frac{\pi}{2}$', r'$\pi$',
                        r'$\frac{3\pi}{2}$', r'$2\pi$'])
    ax.grid(linestyle="--")
    # ax.set_title(str(r"Wang-Buszaki, $I$=%5.2f, $\phi=$%.1f, f=%.1f[Hz]" % (
    #     I_app, phi, S['frequency'])), size=12)
    ax.legend(loc='best', frameon=False, fontsize=12)
    pl.savefig('../data/prc.png', dpi=150)
