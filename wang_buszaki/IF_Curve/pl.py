import numpy as np 
import pylab as pl 


c = np.load('fig/data.npz')
Iext = c['I']
freq = c['freq']

fig, ax = pl.subplots(1, figsize=(6, 5))
ax.plot(Iext[1:], freq[1:], lw=2, c="k")
ax.set_xlabel(r"$I_{app}(\mu A/cm^2)$", fontsize=16)
ax.set_ylabel("f(Hz)", fontsize=16)
ax.tick_params(labelsize=16)
ax.set_xlim(min(Iext), max(Iext))
ax.set_ylim(0,400)
ax.set_yticks(range(0,500,100))
pl.tight_layout()
pl.savefig('fig/f.pdf')
