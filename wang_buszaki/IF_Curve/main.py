#!/usr/bin/python

"""
Phase response curve of Wang-Buszaki Model
"""
import sys
import lib
import numpy as np
import pylab as pl
from sys import exit
from time import time
from cycler import cycler

#--------------------------------------------------------------#

if __name__ == '__main__':

    x0 = [np.random.uniform(-60.0, -55.0)] +\
        np.random.rand(2).tolist()

    Iext = np.arange(0, 2, 0.1).tolist() + np.arange(2, 20, 1).tolist()

    t0 = np.arange(0, 300, 0.05)
    t1 = np.arange(0, 100, 0.05)
    freq = np.zeros(len(Iext))

    for i in range(len(Iext)):
        t = t0
        if Iext[i] > 5:
            t = t1
        freq[i] = lib.find_frequency(x0, t, Iext[i])

    fig, ax = pl.subplots(1, figsize=(6, 5))
    ax.plot(Iext, freq, lw=2, c="k")
    ax.set_xlabel(r"$I_{app}(\mu A/cm^2)$", fontsize=16)
    ax.set_ylabel("f(Hz)", fontsize=16)
    ax.tick_params(labelsize=16)
    ax.set_xlim(min(Iext), max(Iext))
    ax.set_ylim(0,400)
    ax.set_yticks(range(0,500,100))
    pl.tight_layout()
    np.savez("fig/data", I=Iext, freq=freq)
    pl.savefig('fig/f.pdf')
