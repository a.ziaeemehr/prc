#!/usr/bin/python
'''
Current-Frequency plot of Wang-Buszaki neuron model
'''
import numpy as np
from sys import exit
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from numpy import exp


Cm = 1.0
E_L = -65.0
E_Na = 55.0
E_K = -90.0
g_L = 0.1
g_Na = 35.0
g_K = 9.0
phi = 5.0
dt = 0.01


def find_peaks(x):  # index of peaks
    p = []
    for i in range(1, len(x)-1):
        if ((x[i] > x[i-1]) & (x[i] > x[i+1])):
            p.append(i)
    return p


def ode_sys(x0, t, Iext):
    '''
    define Wang Buszaki Model
    '''
    v, h, n = x0

    alpha_m = -0.1*(v+35.0)/(exp(-0.1*(v+35.0))-1.0)
    alpha_h = 0.07*exp(-(v+58.0)/20.0)
    alpha_n = -0.01*(v+34.0)/(exp(-0.1*(v+34.0))-1.0)

    beta_m = 4.0*exp(-(v+60.0)/18.0)
    beta_h = 1.0/(exp(-0.1*(v+28.0))+1.0)
    beta_n = 0.125*exp(-(v+44.0)/80.0)

    m = alpha_m/(alpha_m+beta_m)
    I_Na = g_Na * m*m*m * h * (v-E_Na)
    I_L = g_L*(v-E_L)
    I_K = g_K * n*n*n*n * (v-E_K)

    vp = -I_Na - I_K - I_L + Iext
    hp = phi * (alpha_h * (1-h) - beta_h * h)
    np = phi * (alpha_n * (1-n) - beta_n * n)

    return [vp, hp, np]


# ------------------------------------------------
def find_frequency(x0, t, I):
    lc = odeint(ode_sys, x0, t, args=(I,))    # find limit cycle
    
    # plt.figure()
    
    plt.plot(t, lc[:, 0])
    
    # figname = "%g.png" % I
    # plt.savefig("fig/"+figname, format='png')
    # plt.close()
    
    peak = find_peaks(lc[:, 0])  # index of peaks
    # print peak
    if (len(peak) > 2):
        try:
            period = t[peak[-1]]-t[peak[len(peak)-2]]
            freq = 1./period * 1000
            print I, freq
            return freq
        except:
            raise Exception('There has been an error ')
    #x0 = lc[peak[len(peak)-3]]
    else:
        return 0
# -------------------------------------------------
