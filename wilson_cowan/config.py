import numpy as np



# strength of stimulation
A = np.array([[i/1000.0,0] for i in range(1,11,2)])
tau = 1./128
dphi = 0.2

params = [16.0,     #wEE    0
          12.0,     #wEI    1
          15.0,     #wIE    2
          3.0,      #wII    3
          1.7,     #hE     4
          0.0,      #hI     5
          1.3,      #aE     6
          2.0,      #aI     7
          4.0,      #thetaE 8
          3.7,      #thetaI 9
          1.0,      #rE     10 
          1.,       #rI     11
          8.0,      #tauE   12
          8.0       #tauI   13
          ]
kE = 1.-1./(1.+np.exp(params[6]  * params[8]))
kI = 1.-1./(1.+np.exp(params[7]  * params[9]))
params.append(kE)
params.append(kI)

k   = 3.5
x0 = [0.2,0]                  #a guess for x0