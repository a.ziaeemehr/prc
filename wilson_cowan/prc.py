#!/usr/bin/python
'''
PRC of Wilson-Cowan Model
'''
import numpy as np
from scipy.integrate import odeint
import pylab as pl
import time
from modules import *
from config import *
from sys import exit

start = time.time()
x0,T = find_init_on_LC_period(x0,params,tau)
print 'E0 = %.3f, I0 = %.3f, T = %.2f '%(x0[0],x0[1],T)

t  = np.arange(0,k*T+tau,tau)
tc = np.arange(0,k*T+tau,tau)
lc = odeint(fsys,x0,t,args=(params,))    # find limit cycle

peak  = find_peaks(lc[:,0])   #find index of peaks 
t_ref = tc[peak[-1]]          # the last peak is used for reference

phases = np.arange(0, T + 0.1, dphi)
n1 = len(A[:,0])
n2 = len(phases)

PRC = np.zeros((n1,n2))

for i in range(n1):
    for j in range(n2):
        temp1 = abs(phases[j]-tc)
        m = min(temp1)            #value of min
        l = np.argmin(temp1)      #index of min
        t = np.arange(phases[j],k*T+tau,tau)
        x = odeint(fsys, lc[l,:] + A[i,:], t,args=(params,))   #stimulation
        peaks = find_peaks(x[:,0])
        prc = (((T/2.+t_ref-t[peaks[-1]])%T)-T/2)
        PRC[i,j] = prc

phases = phases *2*np.pi/float(T)   #rescale to [0,2pi]
# saving_data(phases,PRC)
ploting(tc,lc,phases,PRC,A)

print 'Done in %.2f seconds' % (time.time()-start)

pl.show()
