#!/usr/bin/python
import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt
#-------------------------------------------------#
def fsys(init,t,params):    # W-C differential equations
    wEE = params[0]; wEI = params[1]; wIE = params[2]
    wII = params[3]; hE  = params[4]; hI  = params[5]
    aE = params[6];  aI  = params[7]; thetaE = params[8]
    thetaI=params[9];rE = params[10]; rI = params[11]
    tauE = params[12]; tauI = params[13]
    kE = params[14]; kI = params[15]
    
    x , y = init        # x --> E, y --> I
    sE = wEE*x - wEI*y + hE
    sI = wIE*x - wII*y + hI
    dx =1./tauE * ( -x + (kE-rE*x)*Resp(sE,aE,thetaE))
    dy =  1./tauI * ( -y + (kI-rI*y)*Resp(sI,aI,thetaI))
    return dx,dy
#-------------------------------------------------#
def Resp(x,a,theta):               #Response function
    S = 1./(1+ np.exp(-a*(x-theta)))-1./(1.+np.exp(a*theta))
    return S
#-------------------------------------------------#
def find_peaks(x):
    p = []
    for i in range(1,len(x)-1):
        if ( (x[i]>x[i-1]) & (x[i]>x[i+1]) ):
            p.append(i)
    return p    
#-------------------------------------------------#
def find_init_on_LC_period(x0,params,tau):
    t  = np.arange(0,400,tau)    
    lc = odeint(fsys,x0,t,args=(params,))    # find limit cycle
    #plt.plot(t,lc[:,0])
    #plt.show()
    peak = find_peaks(lc[:,0])    #index of peaks
    try:
        period = t[peak[-1]]-t[peak[len(peak)-2]]
    except:
        raise Exception('There has been an error ')
    x0 = lc[peak[len(peak)-3]]
    return x0,period
#-------------------------------------------------
def ploting(tc,lc,phases,PRC,A):
    plt.figure(1)
    plt.plot(tc,lc[:,0])
    plt.xlabel('time'); plt.ylabel('$E$')    
    plt.savefig("./results/1.png")

    plt.figure(2)
    nrow,ncol = PRC.shape
    for i in range(nrow):
        label = str(A[i,0])
        plt.plot(phases,PRC[i,:],label=label)
    plt.xlabel('phase of stimulation'); 
    plt.ylabel('induced phase difference')    
    plt.legend(frameon=False)
    plt.savefig('./results/prc.png')
    
    # plt.show()

#-------------------------------------------------#
def saving_data(phases,PRC):
    # result.txt : first col phase, other prc of Currents 
    #              phase[i], prc_I1[i], prc_I2[i], ... 
    ofile = open('./results/result.txt','w')
    nrow,ncol = PRC.shape

    for j in range(ncol):
        ofile.write('%15.9f  '% phases[j])
        for i in range(nrow):
            ofile.write(' %15.9f '% PRC[i,j])
        ofile.write('\n')
    ofile.close()






#def find_frequency(t,dt,y):
    #'''
    ## N Number of samplepoints
    ## dt sample spacing
    #'''
    #N = len(t)
    #dt = dt * 1.e-3
    #xf = np.linspace(0.0, 1.0/(2.0*dt), N/2)
    #yf = scipy.fftpack.fft(y)
    #y2f = 2.0/N * np.abs(yf[:N//2])
    #plt.plot(xf, y2f)
    #plt.xlim([0,100])
    ## select a proper interval
    #yout = y2f[ (10<xf) & (xf<100) ]
    #xout =  xf[ (10<xf) & (xf<100) ]

    #for index, item in enumerate(yout):
           #if (item == max(yout)):
              #ind,it = index, item
    #return xout[ind]

