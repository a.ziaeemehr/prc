# HH model
import numpy as np
import matplotlib.pylab as plt
from scipy.integrate import odeint
import matplotlib.gridspec as gridspec
from math import exp

# constants----------------------------------------------------
g_Na = 24  # (micro A/mV)/cm^2
g_Kdr = 3  # (micro A/mV)/cm^2
g_Ks = 0.0
g_L = 0.02  # (micro A/mV)/cm^2
V_Na = 55.0  # mV
V_K = -90.0  # mV
V_L = -60.0  # mv
C = 1.0  # micro F/cm^2
alpha_s = 1.0
alpha_h = 1.0
I_dr = -0.10

t0 = 0.0
tn = 100.0
dt = 0.01


def initialize(v0):
    h0 = h_inf(v0)
    n0 = n_inf(v0)
    s0 = s_inf(v0)

    return (v0, h0, n0, s0)


def h_inf(v):
    return (1.0/(1.0 + exp((v+53.0)/7.0)))
def m_inf(v):
    return (1.0/(1.0 + exp((-v-30.0)/9.5)))
def s_inf(v):
    return (1.0/(1.0 + exp((-v-39.0)/5.0)))
def n_inf(v):
    return (1.0/(1.0 + exp((-v-30.0)/10.0)))
def tau_h(v):
    return 0.37 + 2.78/(1.0 + exp((v + 40.5)/6.0))
def tau_n(v):
    return 0.37 + 1.85/(1.0 + exp((v + 27.0)/15.0))
def ode_sys(x, t):
    v, h, n, s = x
    dvdt = -g_Na * m_inf(v) * h * (v-V_Na) -\
        g_Kdr * n**4 * (v-V_K) -\
        g_Ks * s * (v-V_K) -\
        g_L * (v - V_L) + I_dr
    dhdt = alpha_h * (h_inf(v) - h)/tau_h(v)
    dndt = (n_inf(v)-h) / tau_n(v)
    dsdt = alpha_s * (s_inf(v) - s) / 75.0

    return (dvdt, dhdt, dndt, dsdt)

# -------------------------------------------------------------
t = np.arange(t0, tn, dt)
x0 = initialize(-75)
print x0
sol = odeint(ode_sys, x0, t)

# ploting----------------------------------------------------

fig, ax = plt.subplots(2, figsize=(10, 6))

ax[0].plot(t, sol[:, 0])
ax[0].set_ylabel('V')
ax[0].set_xlabel('time(ms)')

ax[1].plot(t, sol[:, 1], '-', label='h')
ax[1].plot(t, sol[:, 2], '-', label='n')
ax[1].plot(t, sol[:, 3], '-', label='s')
ax[1].legend(loc='upper left', frameon=False)
ax[1].set_xlabel('time(ms)')
plt.savefig('../data/f.png')
# -------------------------------------------------------------
