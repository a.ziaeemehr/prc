import numpy as np 
import pylab as pl
import lib


def plot_parameters():

    t0 = 0.0
    tn = 100.0
    dt = 0.01

    t = np.arange(t0, tn, dt)
    x0 = lib.initialize(-75)
    print x0
    sol = odeint(ode_sys, x0, t)

    # ploting----------------------------------------------------

    fig, ax = plt.subplots(2, figsize=(10, 6))

    ax[0].plot(t, sol[:, 0])
    ax[0].set_ylabel('V')
    ax[0].set_xlabel('time(ms)')

    ax[1].plot(t, sol[:, 1], '-', label='h')
    ax[1].plot(t, sol[:, 2], '-', label='n')
    ax[1].plot(t, sol[:, 3], '-', label='s')
    ax[1].legend(loc='upper left', frameon=False)
    ax[1].set_xlabel('Time (ms)')
    plt.savefig('../data/f.png')


def plot_IF_curve():

    Iext = np.arange(-0.10, 1.3, 0.1)
    t = np.arange(0, 500, 0.05)
    freq = np.zeros(len(Iext))
    x0 = lib.initialize(-65.0)
    gks = 1.5

    for i in range(len(Iext)):
        
        freq[i] = lib.find_frequency(x0, t, Iext[i], gks)

    fig, ax = pl.subplots(1, figsize=(6, 5))
    ax.plot(Iext, freq, lw=2, c="k")
    ax.set_xlabel(r"$I_{app}(\mu A/cm^2)$", fontsize=16)
    ax.set_ylabel("f(Hz)", fontsize=16)
    ax.tick_params(labelsize=16)
    ax.set_xlim(min(Iext), max(Iext))
    # ax.set_ylim(0,400)
    # ax.set_yticks(range(0,500,100))
    pl.tight_layout()
    np.savez("../data/IF", I=Iext, freq=freq)
    pl.savefig('../data/f.png')


plot_parameters()