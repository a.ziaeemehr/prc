#!/usr/bin/python

import numpy as np
from scipy.integrate import odeint
import pylab as pl
from sys import exit
from math import *
from main import *

# constants----------------------------------------------------
g_Na = 24  # (micro A/mV)/cm^2
g_Kdr = 3  # (micro A/mV)/cm^2
# g_Ks = 0.1
g_L = 0.02  # (micro A/mV)/cm^2
V_Na = 55.0  # mV
V_K = -90.0  # mV
V_L = -60.0  # mv
C = 1.0  # micro F/cm^2
alpha_s = 1.0
alpha_h = 1.0

def initialize(v0):
    h0 = h_inf(v0)
    n0 = n_inf(v0)
    s0 = s_inf(v0)

    return (v0, h0, n0, s0)


def h_inf(v):
    return (1.0/(1.0 + exp((v+53.0)/7.0)))


def m_inf(v):
    return (1.0/(1.0 + exp((-v-30.0)/9.5)))


def s_inf(v):
    return (1.0/(1.0 + exp((-v-39.0)/5.0)))


def n_inf(v):
    return (1.0/(1.0 + exp((-v-30.0)/10.0)))


def tau_h(v):
    return 0.37 + 2.78/(1.0 + exp((v + 40.5)/6.0))


def tau_n(v):
    return 0.37 + 1.85/(1.0 + exp((v + 27.0)/15.0))


def ode_sys(x, t, I_dr, g_Ks):
    v, h, n, s = x
    dvdt = -g_Na * m_inf(v) * h * (v-V_Na) -\
        g_Kdr * n**4 * (v-V_K) -\
        g_Ks * s * (v-V_K) -\
        g_L * (v - V_L) + I_dr
    dhdt = alpha_h * (h_inf(v) - h)/tau_h(v)
    dndt = (n_inf(v)-h) / tau_n(v)
    dsdt = alpha_s * (s_inf(v) - s) / 75.0

    return (dvdt, dhdt, dndt, dsdt)

# -------------------------------------------------


def find_peaks(x):
    ''' 
    return indices of peaks on array x
    '''
    p = []
    for i in range(1, len(x)-1):
        if ((x[i] > x[i-1]) & (x[i] > x[i+1])):
            p.append(i)
    return p
# -------------------------------------------------


def find_frequency(x0, t, I, gk):
    lc = odeint(ode_sys, x0, t, args=(I, gk))    # find limit cycle

    # plt.figure()

    # plt.plot(t, lc[:, 0])
    # figname = "%g.png" % I
    # plt.savefig("fig/"+figname, format='png')
    # plt.close()

    peak = find_peaks(lc[:, 0])  # index of peaks
    # print peak
    if (len(peak) > 2):
        try:
            period = t[peak[-1]]-t[peak[len(peak)-2]]
            freq = 1./period * 1000
            print I, freq
            return freq
        except:
            raise Exception('There has been an error ')
    else:
        return 0
# -------------------------------------------------


def find_init_on_LC_period(x0, I, gk):
    """
    find a poind on limit cycle
    return : x[1x4] and period or oscillation
             for system fsys
    """
    t = np.arange(0, 60, tau)
    lc = odeint(fsys, x0, t, args=(I, gk))  # find limit cycle
    # pl.plot(t,lc[:,0])
    # pl.show()
    # exit(0)
    peak = find_peaks(lc[:, 0])  # index of peaks
    try:
        period = t[peak[-1]]-t[peak[len(peak)-2]]
    except:
        raise Exception('There has been an error ')
    x0 = lc[peak[len(peak)-3]]
    return x0, period
# -------------------------------------------------
