#!/usr/bin/python
# PRC of Hudgkin-Huxley model
# 2D plot for various I_stimulation
import numpy as np
from scipy.integrate import odeint
from mpl_toolkits.mplot3d import Axes3D
import pylab as pl
from sys import exit
from math import *
from functions import *

# -----Constants ----- #
gbar_Na = 120  # (micro A/mV)/cm^2
gbar_K = 36  # (micro A/mV)/cm^2
gbar_L = 0.3  # (micro A/mV)/cm^2
E_Na = 50.0  # mV
E_K = -77.0  # mV
E_L = -54.6  # mv
C = 1.0  # micro F/cm^2
tau = 2 * pi / 6000
# -----Constants ----- #


phases = np.arange(0, 2. * np.pi + 0.1, 0.1)
T = phases[-1]  # T is the period of the cycle(will be calculated)
k = 3
x0 = initialize()  # a guess for x0
A = [0.5, 0., 0., 0.]     # strength of stimulation
I = np.arange(7.5, 72.5, 20)
PRC = []
PHASES = []
for I_s in I:
    x0, T = find_init_on_LC_period(x0, I_s)
    print 'Period: ', T
    t = np.arange(0, k * T + tau, tau)
    tc = np.arange(0, k * T + tau, tau)
    lc = odeint(fsys, x0, t, args=(I_s,))    # find limit cycle
    peak = find_peaks(lc[:, 0])
    peak0 = tc[peak[-1]]      # the last peak is used for reference
    phases = np.arange(0, T + 0.2, 0.2)
    PHASES.append(phases)
    prc = []
    for i in range(len(phases)):
        temp1 = abs(phases[i] - tc)
        m, j = min(temp1), np.argmin(temp1)  # value and index of min
        t = np.arange(phases[i], k * T + tau, tau)
        x = odeint(fsys, lc[j, :] + A, t, args=(I_s,))  # stimulate
        peaks = find_peaks(x[:, 0])
        prc.append(((T / 2. + peak0 - t[peaks[-1]]) % T) - T / 2)
    PRC.append(prc)
for i in range(len(PRC)):
    pl.plot(PHASES[i][:], PRC[i][:])
pl.xlabel('phase of stimulation')
pl.ylabel('induced phase difference')

fig = pl.figure()
ax = fig.add_subplot(111, projection='3d')

for i in range(len(PRC)):
    b = np.ones(len(PRC[i][:])) * I[i]
    ax.plot(b, PHASES[i][:], PRC[i][:])
ax.set_xlabel('Current(uA/cm2)')
ax.set_ylabel('Phase(ms)')
ax.set_zlabel('PRC')
ax.view_init(25, -35)

pl.show()
