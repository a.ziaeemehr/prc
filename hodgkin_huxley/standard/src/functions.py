#!/usr/bin/python

import numpy as np
from scipy.integrate import odeint
import pylab as pl
from sys import exit
from math import *
from config import *

#-------------------------------------------------------------        
def coefficients(v):
    alph_m  = 0.1*(v + 40.)/(1-exp(-(v + 40.0)/10.0))
    beta_m  = 4.0*exp(-(v + 65.0)/18.0)
    alph_n  = 0.01*(v + 55.0)/(1.0 - exp(-(v + 55.0)/10.0))
    beta_n  = 0.125*exp(-(v +65.0)/80.0)
    alph_h  = 0.07*exp(-(v + 65.0)/20.0)
    beta_h  = 1.0/(1.0 + exp(-(v + 35.0)/10.0))
    J = [alph_m,beta_m,alph_n,beta_n,alph_h,beta_h]
    return J
#-------------------------------------------------------------    
def initialize():
    x = [0., 0., 0., 0.]
    x[0] = -65.0;                              #V
    J = coefficients(x[0]);                        
    alph_m,beta_m,alph_n,beta_n,alph_h,beta_h = J
    x[1] = alph_m /(alph_m + beta_m);          #m
    x[2] = alph_n /(alph_n + beta_n);          #n
    x[3] = alph_h /(alph_h + beta_h);          #h
    return x
#-------------------------------------------------------------
def fsys(x,t,I_s):
    # u_dot(u,m,n,h) u,m,n,h -> 0,1,2,3
    J = coefficients(x[0])
    alph_m,beta_m,alph_n,beta_n,alph_h,beta_h = J
    vp = -(gbar_Na* x[1]**3 * x[3]*(x[0]-E_Na)+ \
    gbar_K * x[2]**4 *(x[0]-E_K) + \
    gbar_L * (x[0]-E_L)) + I_s                        #V dot
    mp = alph_m * (1-x[1])- beta_m * x[1]               #m dot
    np = alph_n * (1-x[2])- beta_n * x[2]               #n dot
    hp = alph_h * (1-x[3])- beta_h * x[3]               #h dot
    return vp,mp,np,hp
#-------------------------------------------------
def find_peaks(x):
    ''' 
    return indices of peaks on array x
    '''
    p = []
    for i in range(1,len(x)-1):
        if ( (x[i]>x[i-1]) & (x[i]>x[i+1]) ):
            p.append(i)
    return p    
#-------------------------------------------------
def find_init_on_LC_period(x0,I_s):
    """
    find a poind on limit cycle
    return : x[1x4] and period or oscillation
             for system fsys
    """
    t  = np.arange(0,60,tau)    
    lc = odeint(fsys,x0,t,args=(I_s,))        #find limit cycle
    #pl.plot(t,lc[:,0])
    #pl.show()
    #exit(0)
    peak = find_peaks(lc[:,0])    #index of peaks
    try:
        period = t[peak[-1]]-t[peak[len(peak)-2]]
    except:
        raise Exception('There has been an error ')
    x0 = lc[peak[len(peak)-3]]
    return x0,period
#-------------------------------------------------