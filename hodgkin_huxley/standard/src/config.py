#!/usr/bin/python
# PRC of Hudgkin-Huxley model 
# 2D plot for one I_stimulation

import numpy as np
from math import pi
#constants----------------------------------------------------
gbar_Na= 120;        #(micro A/mV)/cm^2
gbar_K= 36;          #(micro A/mV)/cm^2
gbar_L= 0.3;         #(micro A/mV)/cm^2
E_Na= 50.0;          #mV
E_K = -77.0;         #mV
E_L = -54.6;         #mv
C = 1.0;             #micro F/cm^2
#-------------------------------------------------------------        
I_sti = 10.0
tau = 2*pi/6000