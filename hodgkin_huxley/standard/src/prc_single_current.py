#!/usr/bin/python
# PRC of Hudgkin-Huxley model
# 2D plot for one I_stimulation
import numpy as np
from scipy.integrate import odeint
import pylab as pl
from sys import exit
from math import *
from functions import *


# -----Constants ----- #
gbar_Na = 120  # (micro A/mV)/cm^2
gbar_K = 36  # (micro A/mV)/cm^2
gbar_L = 0.3  # (micro A/mV)/cm^2
E_Na = 50.0  # mV
E_K = -77.0  # mV
E_L = -54.6  # mv
C = 1.0  # micro F/cm^2
I_sti = 10.0
tau = 2 * pi / 6000
# -----Constants ----- #


phases = np.arange(0, 2. * np.pi + 0.1, 0.1)
T = phases[-1]  # T is the period of the cycle(will be calculated)
k = 3  # number of periods of limic cycle to be calculated
x0 = initialize()  # a guess for x0 (initial voltage)
x0, T = find_init_on_LC_period(x0, I_sti)

print 'Period: ', T
A = [0.5, 0., 0., 0.]     # strength of stimulation on voltage
t = np.arange(0, k * T + tau, tau)
tc = np.arange(0, k * T + tau, tau)
PRC = []
lc = odeint(fsys, x0, t, args=(I_sti,))    # find limit cycle lc[nstep x 4]
peak = find_peaks(lc[:, 0])
t_ref = tc[peak[-1]]      # the last peak is used for reference
phases = np.arange(0, T + 0.2, 0.2)
# lc[j,:] is new initial condition away from limit cycle,
# and show the position of inserting the stimulus

for i in range(len(phases)):
    tmp_phase = abs(phases[i] - tc)
    m, j = min(tmp_phase), np.argmin(tmp_phase)  # value and index of min
    t = np.arange(phases[i], k * T + tau, tau)
    x = odeint(fsys, lc[j, :] + A, t, args=(I_sti,))  # stimulate
    peaks = find_peaks(x[:, 0])
    PRC.append(((T / 2.0 + t_ref - t[peaks[-1]]) % T) - T / 2)


fig, axs = pl.subplots(2, 2, figsize=(8, 8))
axs[0,0].plot(phases, PRC, lw=2, c='b')
axs[0,0].set_xlabel('phase of stimulation')
axs[0,0].set_ylabel('induced phase difference')
axs[0,0].set_xlim(0, T)

axs[0,1].plot(tc, lc[:, 0])
axs[0,1].plot(t, x[:, 0])
axs[0,1].plot(t[peaks[-1]], x[peaks[-1], 0], 'ro')
axs[0,1].set_xlabel('time')
axs[0,1].set_ylabel('membrane potential')

axs[1,0].plot(x[:, 0], x[:, 1], '-')
axs[1,0].set_xlabel('v')
axs[1,0].set_ylabel('m')
pl.savefig("hh_single.png")

pl.show()
