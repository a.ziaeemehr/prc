# HH model 
# output:  plot t-V
#          plot m,n,h -t


import numpy as np
import matplotlib.pylab as plt
from scipy.integrate import odeint
import matplotlib.gridspec as gridspec
from math import *

#constants----------------------------------------------------
gbar_Na= 120;        #(micro A/mV)/cm^2
gbar_K= 36;          #(micro A/mV)/cm^2
gbar_L= 0.3;         #(micro A/mV)/cm^2
E_Na= 50.0;          #mV
E_K = -77.0;         #mV
E_L = -54.6;         #mv
C = 1.0;             #micro F/cm^2

#stimuling current--------------------------------------------
def I_sti(t):
    if((t>10)):
        return 7.5 #6.4
    else:
        return 0.0
#-------------------------------------------------------------
def coefficients(v):
    alph_n  = 0.01*(v + 55.0)/(1.0 - exp(-(v + 55.0)/10.0))
    alph_m  = 0.1*(v + 40.)/(1-exp(-(v + 40.0)/10.0))
    alph_h  = 0.07*exp(-(v + 65.0)/20.0)
    beta_n  = 0.125*exp(-(v +65.0)/80.0)
    beta_m  = 4.0*exp(-(v + 65.0)/18.0)
    beta_h  = 1.0/(1.0 + exp(-(v + 35.0)/10.0))
    J = [alph_m,beta_m,alph_n,beta_n,alph_h,beta_h]
    return J
#-------------------------------------------------------------    
def initialize():
    x = [0., 0., 0., 0.]
    x[0] = -65.0;                              #V
    J = coefficients(x[0]);                        
    alph_m,beta_m,alph_n,beta_n,alph_h,beta_h = J
    x[1] = alph_m /(alph_m + beta_m);          #m
    x[2] = alph_n /(alph_n + beta_n);          #n
    x[3] = alph_h /(alph_h + beta_h);          #h
    return x
#-------------------------------------------------------------
def ode_sys(x,t):
    # u_dot(u,m,n,h) u,m,n,h -> 0,1,2,3
    J = coefficients(x[0])
    alph_m,beta_m,alph_n,beta_n,alph_h,beta_h = J
    vp = -(gbar_Na* x[1]**3 * x[3]*(x[0]-E_Na)+ \
    gbar_K * x[2]**4 *(x[0]-E_K) + \
    gbar_L * (x[0]-E_L)) + I_sti(t)                     #V dot
    mp = alph_m * (1-x[1])- beta_m * x[1]               #m dot
    np = alph_n * (1-x[2])- beta_n * x[2]               #n dot
    hp = alph_h * (1-x[3])- beta_h * x[3]               #h dot
    return vp,mp,np,hp

#-------------------------------------------------------------
t0 = 0.0
tn = 350.0
t = np.linspace(t0,tn,num=1000)
y = [I_sti(t[i]) for i in range(t.size)]
x = initialize()
print x
sol = odeint(ode_sys,x,t)

#ploting----------------------------------------------------

fig = plt.figure(figsize=(10,6))
gs = gridspec.GridSpec(4, 1)
ax1 = plt.subplot(gs[0:3, 0])
plt.plot(t,sol[:,0])
plt.ylabel('V')
plt.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom='off',      # ticks along the bottom edge are off
    top='off',         # ticks along the top edge are off
    labelbottom='off') # labels along the bottom edge are off
ax2 = plt.subplot(gs[3, 0])
plt.plot(t,y)
plt.ylim([0,max(y)+5])
plt.ylabel('I')
plt.xlabel('time(ms)')
plt.savefig('../data/V.png')
plt.figure()
plt.plot(t,sol[:,1],'-',label='m')
plt.plot(t,sol[:,2],'-',label='n')
plt.plot(t,sol[:,3],'-',label='h')
legend = plt.legend(loc='upper left', frameon=False)
plt.xlabel('time(ms)')
plt.savefig('../data/nmh.png')
# plt.show()
#-------------------------------------------------------------

